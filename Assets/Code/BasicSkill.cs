﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class BasicSkill
{
    public List<int> lineList = new List<int>();
    public List<Vector3> directionList = new List<Vector3>();

    /*
           q  w  e 
        a--|--|--|       randomValue =   0   1   2   3   4   5
        s--|--|--|       line        =   a   s   d   q   w   e
        d--|--|--|

        z = far     6
        x = center  7
        c = near    8
            
        dvalue      =   0       1       2       3       4       5
        direction   =   U       D       R       L       sR      sL
        vector      =  right   left    down     up    forward   back
    */


    public void R()
    {
        lineList.Add(5);
        directionList.Add(Vector3.right);
    }
    public void rR()
    {
        lineList.Add(5);
        directionList.Add(Vector3.left);
    }
    public void cR()
    {
        lineList.Add(4);
        directionList.Add(Vector3.right);
    }
    public void rcR()
    {
        lineList.Add(4);
        directionList.Add(Vector3.left);
    }

    public void L()
    {
        lineList.Add(3);
        directionList.Add(Vector3.left);
    }
    public void rL()
    {
        lineList.Add(3);
        directionList.Add(Vector3.right);
    }
    public void U()
    {
        lineList.Add(0);
        directionList.Add(Vector3.up);
    }
    public void rU()
    {
        lineList.Add(0);
        directionList.Add(Vector3.down);
    }
    public void cU()
    {
        lineList.Add(1);
        directionList.Add(Vector3.up);
    }
    public void rcU()
    {
        lineList.Add(1);
        directionList.Add(Vector3.down);
    }
    public void D()
    {
        lineList.Add(2);
        directionList.Add(Vector3.down);
    }
    public void rD()
    {
        lineList.Add(2);
        directionList.Add(Vector3.up);
    }
    public void F()
    {
        lineList.Add(8);
        directionList.Add(Vector3.back);
    }
    public void rF()
    {
        lineList.Add(8);
        directionList.Add(Vector3.forward);
    }
    public void cF()
    {
        lineList.Add(7);
        directionList.Add(Vector3.back);
    }
    public void rcF()
    {
        lineList.Add(7);
        directionList.Add(Vector3.forward);
    }
    public void B()
    {
        lineList.Add(6);
        directionList.Add(Vector3.forward);
    }
    public void rB()
    {
        lineList.Add(6);
        directionList.Add(Vector3.back);
    }



    public void tR()
    {
        lineList.Add(0);
        lineList.Add(1);
        lineList.Add(2);
        directionList.Add(Vector3.down);
        directionList.Add(Vector3.down);
        directionList.Add(Vector3.down);
    }
    public void tL()
    {
        lineList.Add(0);
        lineList.Add(1);
        lineList.Add(2);
        directionList.Add(Vector3.up);
        directionList.Add(Vector3.up);
        directionList.Add(Vector3.up);
    }

    public void tU()
    {
        lineList.Add(3);
        lineList.Add(4);
        lineList.Add(5);
        directionList.Add(Vector3.right);
        directionList.Add(Vector3.right);
        directionList.Add(Vector3.right);
    }
    public void tD()
    {
        lineList.Add(3);
        lineList.Add(4);
        lineList.Add(5);
        directionList.Add(Vector3.left);
        directionList.Add(Vector3.left);
        directionList.Add(Vector3.left);
    }

    public void tsR()
    {
        lineList.Add(6);
        lineList.Add(7);
        lineList.Add(8);
        directionList.Add(Vector3.back);
        directionList.Add(Vector3.back);
        directionList.Add(Vector3.back);
    }
    public void tsL()
    {
        lineList.Add(6);
        lineList.Add(7);
        lineList.Add(8);
        directionList.Add(Vector3.forward);
        directionList.Add(Vector3.forward);
        directionList.Add(Vector3.forward);
    }

    public void Init()
    {
        lineList.Clear();
        directionList.Clear();
    }
}
