﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


static class LineSign
{
    public const int NONE = -1;
    public const int VERTICAL = 0;
    public const int HORIZONTAL = 1;
    public const int STACKED = 2;
}

public class RubiksCube : MonoBehaviour
{
    private float FLOAT_EPSLION = 1.19209290E-07F;
    FindColor findColor = new FindColor();
    Solver solver = new Solver();

    int angle = 0;                                                    // 현재 Cube가 rotation 된 각도
    public static int rightAngle = 90;                                // rotation 실행시에 기준이 되는 angle
    public static int speed = 6;                                      // rotation Speed 값
    public static int rotationSign = LineSign.NONE;                  // rotation을 실행하는 Flag
    public static int lineSelectSign = LineSign.NONE;                // Line을 선택했는지 확인하는 Flag
    public static bool runningFlag = false;
    
    bool stepFlag = false;

    int solverStep = -1;
    

    // Update is called once per frame
    void Update()
    {
        if(!runningFlag)
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                solverStep = 0;
                //initCubeFlag = true;
            }
            SettingMode();
        }
        if (rotationSign != LineSign.NONE)              // rotation 방향이 정해졌다면
        {
            //Total Rotation            
            if (lineSelectSign == LineSign.NONE)        // lineSelectSign이 None이라면 전체 rotation
            {
                RunTotalRoatation();
            }

            //Line Rotation
            if (lineSelectSign != LineSign.NONE)        // lineSelectSign이 정해졌다면 Line Rotation
            {
                RunLineRotation();
            }
        }
        else
        {
            //RandomSuffle
            if (RandomSuffle.randomCount < RandomSuffle.randomSize
                && RandomSuffle.randomShuffleFlag == true)
            {
                RunRandomShuffle();
            }

            //Solver 실행!
            if (solverStep >= 0)
            {
                //stepFlag가 false면 계속 풀면서 basicSkill 쌓음
                if (stepFlag == false)
                {
                    solver.RunSolver(solverStep);
                    stepFlag = true;
                }

                //solver의 solverStep이 solverStep과 같지 않다면
                if (solver.solverStep != solverStep)
                {
                    if (solver.basicSkill.lineList.Count > 0)
                        SkillRun(solver.basicSkill);
                    else
                        stepFlag = false;
                }
                //solver의 solverStep이 solverStep과 같다면 Step이 끝났으므로 다음 Step으로 넘어감
                else
                {
                    stepFlag = false;
                    ++solverStep;
                }

            }
        }
    }

    void RecomputeANDInit()
    {
        if (angle == rightAngle / speed)            // angle이 90 / speed 까지 증가했다면
        {
            if(lineSelectSign == LineSign.NONE)
                RecomputePositionRotation(unionCube);
            else
                RecomputePositionRotation(LineCube);
            AllInit();                              // Flag들과 angle을 초기화하면서 if문 탈출
            runningFlag = false;
        }
    }
    void SettingMode()
    {
        SetDirection();                     // runningFlag가 false라면 키보드 입력으로 rotation 방향 설정
        SelectLineCube();                   // runningFlag가 false라면 키보드 입력으로 LineCube 설정
        RandomSuffle.RandomSuffleSetting(); // runningFlag가 false라면 키보드 입력으로 RandomShuffle 설정        
    }
    void RunTotalRoatation()
    {
        runningFlag = true;                     // runningFlag를 true로 둬서 키보드 입력을 제한
        Rotation(unionCube, rotationAxis);      // unionCube rotationAxis 기준으로 회전
        ++angle;                                // angle을 1증가
        RecomputeANDInit();
    }
    void RunLineRotation()
    {
        if(lineSelectSign != rotationSign)      // LineCube의 방향과 rotation 방향이 맞지 않다면
        {                                       // ex) 세로 라인을 선택한 뒤 가로로 회전
            Debug.Log("Please Right Setting Line Axis and Rotation Axis!");
            AllInit();
        }
        else                                    // 두 방향이 맞으면
        {
            runningFlag = true;                 // runningFlag를 true로 둬서 키보드 입력을 제한
            Rotation(LineCube, rotationAxis);   // LineCube를 rotationAxis 기준으로 회전
            ++angle;                            // angle을 1증가
            RecomputeANDInit();
        }
          
    }

    static int unionLineThreshold = 7;
    void RunRandomShuffle()
    {
        runningFlag = true;                                 // runningFlag를 true로 둬서 키보드 입력을 제한

        if(RandomSuffle.randomLineUnionChooseValue < unionLineThreshold)    // unionLineThreshold보다 작으면 Line Rotation
        {
            LineSetting(RandomSuffle.randomLineValue);      // Line 선택
            RandomSuffle.RandomDirectionSetting();          // Line 및 Direction 설정
            Rotation(LineCube, rotationAxis);               // LineCube를 rotationAxis 기준으로 회전
            ++angle;                                        // angle을 1증가
        }
        else
        {
            Rotation(unionCube, rotationAxis);              // unionCube rotationAxis 기준으로 회전
            ++angle;                                        // angle을 1증가
        }

        if (angle == rightAngle / speed)                    // angle이 90 / speed 까지 증가했다면
        {
            if(RandomSuffle.randomLineUnionChooseValue < unionLineThreshold)
                RecomputePositionRotation(LineCube);
            else
                RecomputePositionRotation(unionCube);
                
            AllInit();                                      // Flag들과 angle을 초기화
            ++RandomSuffle.randomCount;                     // randomCount 증가
            RandomSuffle.RandomValueSetting();              // random값들 재정의
            if(RandomSuffle.randomCount == RandomSuffle.randomSize)         // randomCount가 randomSize에 도달했다면
            {
                RandomSuffle.randomCount = 0;               // randomCount 초기화
                RandomSuffle.randomShuffleFlag = false;     // flag 초기화
                runningFlag = false;                        // runningflag를 false로 바꾸어 키보드 입력 허가 
            }
        }
    }

    int lineListCount = 0;
    public void SkillRun(BasicSkill bS)
    {
        runningFlag = true;                                             // runningFlag를 true로 둬서 키보드 입력을 제한

        LineSetting(bS.lineList[lineListCount]);                // 저장되어 있는 lineList에서 값을 가져와 Line 설정
        rotationAxis = bS.directionList[lineListCount];         // 저장되어 있는 DirectionList에서 값을 가져와 rotationAxis 설정
        Rotation(LineCube, rotationAxis);                               // Rotation
        ++angle;

        if (angle == rightAngle / speed)                                // angle이 90 / speed 까지 증가했다면
        {
            RecomputePositionRotation(LineCube);                        // 위치 및 각도 보정
            AllInit();                                                  // 값 초기화
            ++lineListCount;                                            // lineListCount + 1
            if (lineListCount == bS.lineList.Count)             // lineList의 마지막까지 돌았다면
            {
                stepFlag = false;
                lineListCount = 0;                                      // lineList Count 초기화
                runningFlag = false;                                    // runningFlag 초기화   
            }
        }
    }




    public static void LineSetting(int lineSelectValue)
    {
        
        float FLOAT_EPSLION_ = 1.19209290E-07F;
        /*
               q  w  e 
            a--|--|--|       randomValue =   0   1   2   3   4   5
            s--|--|--|       line        =   a   s   d   q   w   e
            d--|--|--|

            z = far     6
            x = center  7
            c = near    8
        */
        if (unionCube_ != null)
        { 
            if(lineSelectValue == 0)
            {
                // Cube 중 y position이 1에 있는 Cube 선택
                LineCube = unionCube_.FindAll(x => Math.Abs(1 - x.transform.GetChild(0).position.y) < FLOAT_EPSLION_);
                lineSelectSign = LineSign.HORIZONTAL;
            }
            else if(lineSelectValue == 1)
            {
                // Cube 중 y position이 0에 있는 Cube 선택
                LineCube = unionCube_.FindAll(x => Math.Abs(0 - x.transform.GetChild(0).position.y) < FLOAT_EPSLION_);
                lineSelectSign = LineSign.HORIZONTAL;
            }
            else if (lineSelectValue == 2)
            {
                // Cube 중 x position이 -1에 있는 Cube 선택
                LineCube = unionCube_.FindAll(x => Math.Abs(-1 - x.transform.GetChild(0).position.y) < FLOAT_EPSLION_);
                lineSelectSign = LineSign.HORIZONTAL;
            }
            else if (lineSelectValue == 3)
            {
                // Cube 중 x position이 1에 있는 Cube 선택
                LineCube = unionCube_.FindAll(x => Math.Abs(-1 - x.transform.GetChild(0).position.x) < FLOAT_EPSLION_);
                lineSelectSign = LineSign.VERTICAL;
            }
            else if (lineSelectValue == 4)
            {
                // Cube 중 x position이 0에 있는 Cube 선택
                LineCube = unionCube_.FindAll(x => Math.Abs(0 - x.transform.GetChild(0).position.x) < FLOAT_EPSLION_);
                lineSelectSign = LineSign.VERTICAL;
            }
            else if (lineSelectValue == 5)
            {
                // Cube 중 x position이 -1에 있는 Cube 선택
                LineCube = unionCube_.FindAll(x => Math.Abs(1 - x.transform.GetChild(0).position.x) < FLOAT_EPSLION_);
                lineSelectSign = LineSign.VERTICAL;
            }
            else if (lineSelectValue == 6)
            {
                // Cube 중 z position이 1에 있는 Cube 선택
                LineCube = unionCube_.FindAll(x => Math.Abs(1 - x.transform.GetChild(0).position.z) < FLOAT_EPSLION_);
                lineSelectSign = LineSign.STACKED;
            }
            else if (lineSelectValue == 7)
            {
                // Cube 중 z position이 0에 있는 Cube 선택
                LineCube = unionCube_.FindAll(x => Math.Abs(0 - x.transform.GetChild(0).position.z) < FLOAT_EPSLION_);
                lineSelectSign = LineSign.STACKED;
            }
            else if (lineSelectValue == 8)
            {
                // Cube 중 z position이 -1에 있는 Cube 선택
                LineCube = unionCube_.FindAll(x => Math.Abs(-1 - x.transform.GetChild(0).position.z) < FLOAT_EPSLION_);
                lineSelectSign = LineSign.STACKED;
            }
        }
    }


    //SetDirection 함수 
    /*
        키보드 입력을 받아서 rotationAxis 설정
    */
    public static Vector3 rotationAxis;                       // Cube의 roation 축
    void SetDirection()
    {
        int directionSelectValue = -1;
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            directionSelectValue = 0;
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            directionSelectValue = 1;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            directionSelectValue = 2;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            directionSelectValue = 3;
        }
        else if (Input.GetKeyDown(KeyCode.Period))
        {
            directionSelectValue = 4;
        }
        else if (Input.GetKeyDown(KeyCode.Comma))
        {
            directionSelectValue = 5;
        }
        DirectionSetting(directionSelectValue);
    }

    void DirectionSetting(int directionSelectValue)
    {
        if(directionSelectValue == 0)
        {
            rotationAxis = Vector3.right;
            rotationSign = LineSign.VERTICAL;
        }
        else if(directionSelectValue == 1)
        {
            rotationAxis = Vector3.left;
            rotationSign = LineSign.VERTICAL;
        }
        else if (directionSelectValue == 2)
        {
            rotationAxis = Vector3.down;
            rotationSign = LineSign.HORIZONTAL;
        }
        else if (directionSelectValue == 3)
        {
            rotationAxis = Vector3.up;
            rotationSign = LineSign.HORIZONTAL;
        }
        else if (directionSelectValue == 4)
        {
            rotationAxis = Vector3.back;
            rotationSign = LineSign.STACKED;
        }
        else if (directionSelectValue == 5)
        {
            rotationAxis = Vector3.forward;
            rotationSign = LineSign.STACKED;
        }
    }
    //Rotation 함수 
    /*
        입력으로 받은 cube를 dirVec축으로 회전
    */
    void Rotation(List<GameObject> cube, Vector3 dirVec)
    {
        for (int i = 0; i < cube.Count; ++i)
        {
            for (int numchild = 0; numchild < cube[i].transform.childCount; ++numchild)
            {
                cube[i].transform.GetChild(numchild).RotateAround(Vector3.zero, dirVec, speed);
            }
        }
    }
    


    //RecomputePositionRotation 함수 
    /*
        입력된 Puzzle의 Position 및 Rotation error 보정
    */
    public static void RecomputePositionRotation(List<GameObject> cube)
    {
        SettingPosition(cube);
        SettingRotation(cube);
    }

    //SettingPosition 함수 
    /*
        Position 값의 Floating Error를 보정
        보정하지 않으면 error값에 의해 위치가 계속 틀어짐
    */
    public static void SettingPosition(List<GameObject> cube)
    {
        for (int i = 0; i < cube.Count; ++i)
        {
            for (int numchild = 0; numchild < cube[i].transform.childCount; ++numchild)
            {
                // 소수점 첫번째 자리 반올림
                cube[i].transform.GetChild(numchild).position
                 = new Vector3(
                     (float)Math.Round(cube[i].transform.GetChild(numchild).position.x, 1),
                     (float)Math.Round(cube[i].transform.GetChild(numchild).position.y, 1),
                     (float)Math.Round(cube[i].transform.GetChild(numchild).position.z, 1)
                 );
            }
        }
    }

    //SettingRotation 함수 
    /*
        Rotation 값의 Floating Error를 보정
        보정하지 않으면 error값에 의해 회전값이 계속 틀어짐
    */
    public static void SettingRotation(List<GameObject> cube)
    {
        for (int i = 0; i < cube.Count; ++i)
        {
            for (int numchild = 0; numchild < cube[i].transform.childCount; ++numchild)
            {
                float xRot = RotationValueCheck(cube[i].transform.GetChild(numchild).eulerAngles.x);
                float yRot = RotationValueCheck(cube[i].transform.GetChild(numchild).eulerAngles.y);
                float zRot = RotationValueCheck(cube[i].transform.GetChild(numchild).eulerAngles.z);
                
                cube[i].transform.GetChild(numchild).eulerAngles = new Vector3(xRot, yRot, zRot);
                
            }
        }
    }
    //RotationValueCheck 함수 
    /*
        Floating Error를 보정해주기 위해 값을 재설정
    */
    public static float RotationValueCheck(float rot)
    {
        float absRot = Math.Abs(rot);
        if (absRot >= 45 && absRot < 135)
            absRot = 90;
        else if(absRot >= 135 && absRot < 225)
            absRot = 180;
        else if (absRot >= 225 && absRot < 315)
            absRot = 270;
        else if (absRot >= 315 || absRot < 45)
            absRot = 0;

        if (rot < 0)
            return -absRot;
        else
            return absRot;
    }






    //SelectLineCube 함수 
    /*
        lpf 값에 따른 LinePuzzle 선택 후 Return
    */
    
    public static List<GameObject> LineCube;
    void SelectLineCube()
    {
        /*
               q  w  e 
            a--|--|--|       randomValue =   0   1   2   3   4   5
            s--|--|--|       line        =   a   s   d   q   w   e
            d--|--|--|

            z = far     6
            x = center  7
            c = near    8
        */
        if (unionCube != null)
        {
            if(Input.GetKeyDown(KeyCode.A))
            {
                // Cube 중 y position이 1에 있는 Cube 선택
                LineCube = unionCube.FindAll(x => Math.Abs(1 - x.transform.GetChild(0).position.y) < FLOAT_EPSLION);
                lineSelectSign = LineSign.HORIZONTAL;
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                // Cube 중 y position이 0에 있는 Cube 선택
                LineCube = unionCube.FindAll(x => Math.Abs(0 - x.transform.GetChild(0).position.y) < FLOAT_EPSLION);
                lineSelectSign = LineSign.HORIZONTAL;
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                // Cube 중 y position이 -1에 있는 Cube 선택
                LineCube = unionCube.FindAll(x => Math.Abs(-1 - x.transform.GetChild(0).position.y) < FLOAT_EPSLION);
                lineSelectSign = LineSign.HORIZONTAL;
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                // Cube 중 x position이 1에 있는 Cube 선택
                LineCube = unionCube.FindAll(x => Math.Abs(1 - x.transform.GetChild(0).position.x) < FLOAT_EPSLION);
                lineSelectSign = LineSign.VERTICAL;
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                // Cube 중 x position이 0에 있는 Cube 선택
                LineCube = unionCube.FindAll(x => Math.Abs(0 - x.transform.GetChild(0).position.x) < FLOAT_EPSLION);
                lineSelectSign = LineSign.VERTICAL;
            }
            else if (Input.GetKeyDown(KeyCode.Q))
            {
                // Cube 중 x position이 -1에 있는 Cube 선택
                LineCube = unionCube.FindAll(x => Math.Abs(-1 - x.transform.GetChild(0).position.x) < FLOAT_EPSLION);
                lineSelectSign = LineSign.VERTICAL;
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                // Cube 중 z position이 1에 있는 Cube 선택
                LineCube = unionCube.FindAll(x => Math.Abs(1 - x.transform.GetChild(0).position.z) < FLOAT_EPSLION);
                lineSelectSign = LineSign.STACKED;
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                // Cube 중 z position이 0에 있는 Cube 선택
                LineCube = unionCube.FindAll(x => Math.Abs(0 - x.transform.GetChild(0).position.z) < FLOAT_EPSLION);
                lineSelectSign = LineSign.STACKED;
            }
            else if (Input.GetKeyDown(KeyCode.C))
            {
                // Cube 중 z position이 -1에 있는 Cube 선택
                LineCube = unionCube.FindAll(x => Math.Abs(-1 - x.transform.GetChild(0).position.z) < FLOAT_EPSLION);
                lineSelectSign = LineSign.STACKED;
            }
        }

        return ;        
    }





    //AllInit 함수 
    /*
        Flag 및 angle 초기화
    */
    void AllInit()
    {
        rotationSign = LineSign.NONE;
        lineSelectSign = LineSign.NONE;
        angle = 0;
        LineCube = null;
    }
    
    //MakeACube 함수 
    /*
        입체감 있는 Cube 생성
    */
    public void MakeACube()
    {        
        //Cube 생성
        int count = 0;
        for(int xCoord = -1; xCoord <= 1; ++xCoord)
        {
            for(int yCoord = -1; yCoord <= 1; ++yCoord)
            {
                for(int zCoord = -1; zCoord <= 1; ++zCoord)
                {
                    CubeInit(xCoord, yCoord, zCoord, count);
                    ++count;
                }   
            }   
        }
        unionCube_ = unionCube;
    }

    GameObject singleCube;
    public List<GameObject> unionCube;
    public static List<GameObject> unionCube_;
    public void CubeInit(float xCoor, float yCoor, float zCoor, int count)
    {
        singleCube = new GameObject("singleCube" + count);
        float e = 0.1f;

        //Resources 폴더에서 불러오기
        GameObject centerCube = Resources.Load<GameObject>("Object/CenterCube");
        GameObject topCube = Resources.Load<GameObject>("Object/TopCube");
        GameObject bottomCube = Resources.Load<GameObject>("Object/BottomCube");
        GameObject rightCube = Resources.Load<GameObject>("Object/RightCube");
        GameObject leftCube = Resources.Load<GameObject>("Object/LeftCube");
        GameObject farCube = Resources.Load<GameObject>("Object/FarCube");
        GameObject nearCube = Resources.Load<GameObject>("Object/NearCube");

        // 색을 칠해주기 위해 0.1만큼 작은 Cube 6개 추가
        GameObject cCube = Instantiate(centerCube, new Vector3(xCoor, yCoor, zCoor), Quaternion.identity);
        GameObject tCube = Instantiate(topCube, new Vector3(xCoor, yCoor+e, zCoor), Quaternion.identity);
        GameObject bCube = Instantiate(bottomCube, new Vector3(xCoor, yCoor-e, zCoor), Quaternion.identity);
        GameObject rCube = Instantiate(rightCube, new Vector3(xCoor+e, yCoor, zCoor), Quaternion.identity);
        GameObject lCube = Instantiate(leftCube, new Vector3(xCoor-e, yCoor, zCoor), Quaternion.identity);
        GameObject fCube = Instantiate(farCube, new Vector3(xCoor, yCoor, zCoor+e), Quaternion.identity);
        GameObject nCube = Instantiate(nearCube, new Vector3(xCoor, yCoor, zCoor-e), Quaternion.identity);
        
        //Color 이름 설정
        cCube.name = "Black";
        tCube.name = "Yellow";
        bCube.name = "White";
        rCube.name = "Blue";
        lCube.name = "Green";
        fCube.name = "Orange";
        nCube.name = "Red";
        
        // 모든 큐브를 하나로 묶어줌
        cCube.transform.parent = singleCube.transform;
        tCube.transform.parent = singleCube.transform;
        bCube.transform.parent = singleCube.transform;
        rCube.transform.parent = singleCube.transform;
        lCube.transform.parent = singleCube.transform;
        fCube.transform.parent = singleCube.transform;
        nCube.transform.parent = singleCube.transform;

        unionCube.Add(singleCube);
        
    }
}
