﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Solver
{
    FindColor findColor = new FindColor();
    public BasicSkill basicSkill = new BasicSkill();
    //Step 순서를 정하는 변수
    public int solverStep = 0;

    public void RunSolver(int step)
    {
        switch(step)
        {
            case 1:
                SetBottomCenterWhiteStep();
                break;
            case 2:
                SetBottomCrossStep();
                break;
            case 3:
                SetBottomSideCubePackStep();
                break;
            case 4:
                SetBottomCornerCubePackStep();
                break;
            case 5:
                SetSecondFloorStep();
                break;
            case 6:
                SetTopCrossStep();
                break;
            case 7:
                SetTopCornerStep();
                break;
            case 8:
                SetThirdFloorSideCubeStep();
                break;
            case 9:
                SetThirdFloorCornerCubeStep();
                break;
        }
    }
    

    int setBottomSideCubePackStepCount = 1;
    private void SetBottomSideCubePackStep()
    {
        if (setBottomSideCubePackStepCount == 1)
        {
            SkilltUtU();
            setBottomSideCubePackStepCount++;
        }
        else if (setBottomSideCubePackStepCount == 2)
        {
            SetBottomSideCube();
        }
    }

    // White를 가운데로 가진 면을 바닥으로 보내는 함수
    private void SetBottomCenterWhiteStep()
    {
        InitBasicSkill();
        string whiteColorFace = findColor.GetFaceName("White");
        if (whiteColorFace == "Top")
        {
            basicSkill.tD();
            basicSkill.tD();
        }
        else if (whiteColorFace == "Bottom")
        {
            solverStep++;
            return ;
        }
        else if (whiteColorFace == "Right")
        {
            basicSkill.tsR();
        }
        else if (whiteColorFace == "Left")
        {
            basicSkill.tsL();
        }
        else if (whiteColorFace == "Far")
        {
            basicSkill.tU();
        }
        else if (whiteColorFace == "Near")
        {
            basicSkill.tD();
        }
    }

    //Bottom 십자가 맞추기
    private void SetBottomCrossStep()
    {
        InitBasicSkill();
        //White Color가 있는 Face와 index 저장
        findColor.FindColorLocation("White");

        int count = 0;
        for (int i = 0; i < 9; ++i)
        {
            //Side Cube만 확인하면 된다. Side Cube의 번호는 모두 홀수
            if(findColor.findColorLocationList[i].Second % 2 == 1)
            {
                //Bottom에 1,3,5,7 중 White Color인 개수를 센다.
                if (findColor.findColorLocationList[i].First == "Bottom")
                {
                    ++count;
                }
                else
                {
                    if (findColor.findColorLocationList[i].First == "Top")
                    {
                        if (findColor.findColorLocationList[i].Second == 1)
                        {
                            basicSkill.U();
                        }
                        else if (findColor.findColorLocationList[i].Second == 3)
                        {
                            basicSkill.U();
                            basicSkill.U();
                        }
                        else if (findColor.findColorLocationList[i].Second == 5)
                        {
                            basicSkill.rU();
                        }
                        else if (findColor.findColorLocationList[i].Second == 7)
                        {
                        }
                        basicSkill.rR();
                        basicSkill.rR();
                    }
                    else if (findColor.findColorLocationList[i].First == "Right")
                    {
                        if (findColor.findColorLocationList[i].Second == 1)
                        {
                            basicSkill.R();
                        }
                        else if (findColor.findColorLocationList[i].Second == 3)
                        {
                            basicSkill.R();
                            basicSkill.R();
                        }
                        else if (findColor.findColorLocationList[i].Second == 5)
                        {
                            basicSkill.rR();
                        }
                        else if (findColor.findColorLocationList[i].Second == 7)
                        {
                        }
                        basicSkill.cU();
                        basicSkill.rR();
                    }
                    else if (findColor.findColorLocationList[i].First == "Left")
                    {
                        if (findColor.findColorLocationList[i].Second == 1)
                        {
                            basicSkill.rU();
                            basicSkill.rU();
                            basicSkill.R();
                            basicSkill.cU();
                        }
                        else if (findColor.findColorLocationList[i].Second == 3)
                        {
                            basicSkill.rcU();
                            basicSkill.rcU();
                            basicSkill.R();
                            basicSkill.R();
                            basicSkill.cU();
                        }
                        else if (findColor.findColorLocationList[i].Second == 5)
                        {
                            basicSkill.rL();
                            basicSkill.rcU();
                        }
                        else if (findColor.findColorLocationList[i].Second == 7)
                        {
                            basicSkill.rcU();
                        }
                        basicSkill.rR();
                    }
                    else if (findColor.findColorLocationList[i].First == "Far")
                    {
                        if (findColor.findColorLocationList[i].Second == 1)
                        {
                            basicSkill.U();
                            basicSkill.R();
                            basicSkill.cU();
                            basicSkill.rR();
                        }
                        else if (findColor.findColorLocationList[i].Second == 3)
                        {
                            basicSkill.R();
                        }
                        else if (findColor.findColorLocationList[i].Second == 5)
                        {
                            basicSkill.B();
                            basicSkill.R();
                        }
                        else if (findColor.findColorLocationList[i].Second == 7)
                        {
                            basicSkill.cU();
                            basicSkill.cU();
                            basicSkill.rR();
                        }
                    }
                    else if (findColor.findColorLocationList[i].First == "Near")
                    {
                        if (findColor.findColorLocationList[i].Second == 1)
                        {
                            basicSkill.rU();
                            basicSkill.R();
                            basicSkill.cU();
                        }
                        else if (findColor.findColorLocationList[i].Second == 3)
                        {
                            basicSkill.rcU();
                            basicSkill.R();
                            basicSkill.R();
                            basicSkill.cU();
                        }
                        else if (findColor.findColorLocationList[i].Second == 5)
                        {
                            basicSkill.rF();
                        }
                        else if (findColor.findColorLocationList[i].Second == 7)
                        {
                        }
                        basicSkill.rR();
                    }
                    basicSkill.D();
                    return;
                }
                
            }
        }
        
        //만약 count가 4라면 십자가가 맞춰졌으므로 다음 Step으로 넘어간다.
        if(count == 4)
        {

            solverStep++;
            return;
        }
    }

    //맨 아랫줄의 Side Cube를 제자리로 맞춤
    private void SetBottomSideCube()
    {
        InitBasicSkill();
        //옆면 색 가져오기
        string rightColor = findColor.GetCubeColor("Right", 8);
        string leftColor = findColor.GetCubeColor("Left", 8);
        string farColor = findColor.GetCubeColor("Far", 8);
        string nearColor = findColor.GetCubeColor("Near", 8);
        
        string rightTopColor = findColor.GetCubeColor("Right", 1);
        string leftTopColor = findColor.GetCubeColor("Left", 1);
        string farTopColor = findColor.GetCubeColor("Far", 1);
        string nearTopColor = findColor.GetCubeColor("Near", 1);

        if((rightColor == rightTopColor) && (leftColor == leftTopColor) &&
            (farColor == farTopColor) && (nearColor == nearTopColor))
        {

            solverStep++;
            return;
        }

        if (rightColor == rightTopColor)
        {
            //Nothing
        }
        else if (rightColor == farTopColor)
        {
            basicSkill.R();
            basicSkill.U();
            basicSkill.rR();
            basicSkill.rU();
            basicSkill.R();
        }
        else if (rightColor == leftTopColor)
        {
            basicSkill.R();
            basicSkill.U();
            basicSkill.U();
            basicSkill.rR();
            basicSkill.U();
            basicSkill.U();
            basicSkill.R();
        }
        else if (rightColor == nearTopColor)
        {
            basicSkill.R();
            basicSkill.rU();
            basicSkill.rR();
            basicSkill.U();
            basicSkill.R();
        }
        basicSkill.tL();
    }


    //맨 아랫줄의 Corner Cube를 제자리로 맞춤
    int setBottomCornerCubeStepCount = 0;
    int setBottomCornerCubeFirstResult = 0;
    private void SetBottomCornerCubePackStep()
    {
        if (setBottomCornerCubeStepCount == 0)
        {
            SkilltUtU();
            setBottomCornerCubeStepCount++;
        }
        else if (setBottomCornerCubeStepCount == 1)
        {
            SetBottomCornerCubeFirstStep();
        }
        else if (setBottomCornerCubeStepCount == 2)
        {
            SetBottomCornerCubeSecondStep(setBottomCornerCubeFirstResult);
        }
        else if (setBottomCornerCubeStepCount == 3)
        {
            SkilltL();
            setBottomCornerCubeStepCount = 1;
        }
    }

    //Corner Cube 3번 자리에 들어가야할 Cube를 찾아서 0번이나 3번에 위치
    private void SetBottomCornerCubeFirstStep()
    {
        InitBasicSkill();
        findColor.SetCubeColorList();
        List<string> checkCube = new List<string>();
        checkCube.Clear();
        checkCube.Add(findColor.GetCubeColor("Near", 8));
        checkCube.Add(findColor.GetCubeColor("Right", 8));
        checkCube.Add(findColor.GetCubeColor("Bottom", 8));
        
        int rightCubeIndex = 0;
        for (int i =0; i< findColor.cornerCubeColorList.Count; ++i)
        {
            if(CheckCubeLocation(findColor.cornerCubeColorList[i], checkCube))
            {
                rightCubeIndex = i;
            }
        }
        
        if (rightCubeIndex == 0 || rightCubeIndex == 3)
        {
            setBottomCornerCubeStepCount++;
            setBottomCornerCubeFirstResult = rightCubeIndex;
            return;
        }


        if (rightCubeIndex == 0)
        {
        }
        else if (rightCubeIndex == 1)
        {
            basicSkill.rU();
        }
        else if (rightCubeIndex == 2)
        {
            basicSkill.rL();
            basicSkill.rU();
            basicSkill.L();
        }
        else if (rightCubeIndex == 3)
        {
        }
        else if (rightCubeIndex == 4)
        {
            basicSkill.rU();
            basicSkill.rU();
        }
        else if (rightCubeIndex == 5)
        {
            basicSkill.U();
        }
        else if (rightCubeIndex == 6)
        {
            basicSkill.B();
            basicSkill.U();
            basicSkill.rB();
        }
        else if (rightCubeIndex == 7)
        {
            basicSkill.rB();
            basicSkill.U();
            basicSkill.U();
            basicSkill.B();
        }




    }


    //cube와 checkCube 간의 색을 비교해서 모든 색이 같으면 true 반환
    private bool CheckCubeLocation(List<string> cube, List<string> checkCube)
    {
        int count = 0;
        for(int i = 0; i< cube.Count; ++i)
        {
            for(int j = 0; j < checkCube.Count; ++j)
            {
                if (cube[i] == checkCube[j])
                    count++;
            }
        }

        if (count == cube.Count)
            return true;

        return false;
    }


    //0번이나 3번에 있는 CornerCube를 3번에 색에 맞춰 넣기
    private void SetBottomCornerCubeSecondStep(int num)
    {
        InitBasicSkill();
        if (SetBottomCornerCubeStepClearCheck())
        {
            setBottomCornerCubeFirstResult = 0;
            solverStep++;
            return;
        }
        if (findColor.GetCubeColor("Bottom", 4) == findColor.GetCubeColor("Bottom", 8) &&
            findColor.GetCubeColor("Right", 4) == findColor.GetCubeColor("Right", 8) &&
            findColor.GetCubeColor("Near", 6) == findColor.GetCubeColor("Near", 8))
        {
            setBottomCornerCubeFirstResult = 0;
            setBottomCornerCubeStepCount++;
            return;
        }
        else
        {
            if (num == 0 && findColor.GetCubeColor("Right", 2) == findColor.bottomColor)
            {
                SkillRUrRrU();
            }
            else if (num == 0 && findColor.GetCubeColor("Near", 0) == findColor.bottomColor)
            {
                basicSkill.tL();
                SkillrLrULU();
                basicSkill.tR();
            }
            else if (num == 0 && findColor.GetCubeColor("Top", 6) == findColor.bottomColor)
            {
                SkillRUrRrU();
                SkillRUrRrU();
                SkillRUrRrU();
            }
            else if (num == 3 && findColor.GetCubeColor("Near", 6) == findColor.bottomColor)
            {
                basicSkill.R();
                basicSkill.rU();
                basicSkill.rR();
                basicSkill.tL();
                SkillrLrULU();
                basicSkill.tR();
            }
            else if (num == 3 && findColor.GetCubeColor("Right", 4) == findColor.bottomColor)
            {
                basicSkill.tL();
                basicSkill.rL();
                basicSkill.U();
                basicSkill.L();
                basicSkill.tR();
                SkillRUrRrU();
            }
        }
    }


    //맨 아랫줄의 CornerCube가 제자리에 위치했는지 확인
    private bool SetBottomCornerCubeStepClearCheck()
    {
        int clearCount = 0;
        for(int i = 0; i< 9; ++i)
        {
            if (findColor.GetCubeColor("Bottom", i) == findColor.bottomColor)
                ++clearCount;
        }
        for(int i = 0; i< 3; ++i)
        {
            if (findColor.GetCubeColor("Right", i + 4) == findColor.GetCubeColor("Right", 8))
                ++clearCount;
            if (findColor.GetCubeColor("Near", i + 4) == findColor.GetCubeColor("Near", 8))
                ++clearCount;
            if (findColor.GetCubeColor("Left", i + 4) == findColor.GetCubeColor("Left", 8))
                ++clearCount;
            if (findColor.GetCubeColor("Far", i + 4) == findColor.GetCubeColor("Far", 8))
                ++clearCount;
        }

        if (clearCount == 21)
            return true;

        return false;
    }



    //2번째 층을 맞춤
    private void SetSecondFloorStep()
    {
        InitBasicSkill();

        if (SecondFlooClearCheck())
        {
            solverStep++;
            return;
        }

        List<string> checkCubeRight = new List<string>();
        checkCubeRight.Clear();
        checkCubeRight.Add(findColor.GetCubeColor("Near", 8));
        checkCubeRight.Add(findColor.GetCubeColor("Right", 8));

        findColor.SetCubeColorList();
        if (CheckCubeLocation(findColor.sideCubeColorList[0], checkCubeRight))
        {
            //Near
            if (findColor.GetCubeColor("Near", 1) == findColor.GetCubeColor("Near", 8))
            {
                SkillSecondFloorRight();
            }
            else
            {
                basicSkill.rU();
                basicSkill.tL();
                SkillSecondFloorLeft();
            }

        }
        else if (CheckCubeLocation(findColor.sideCubeColorList[4], checkCubeRight))
        {
            //Right
            if (findColor.GetCubeColor("Right", 1) == findColor.GetCubeColor("Near", 8))
            {
                basicSkill.U();
                SkillSecondFloorRight();
            }
            else
            {
                basicSkill.tL();
                SkillSecondFloorLeft();
            }
        }
        else if (CheckCubeLocation(findColor.sideCubeColorList[5], checkCubeRight))
        {
            //Left
            if (findColor.GetCubeColor("Left", 1) == findColor.GetCubeColor("Near", 8))
            {
                basicSkill.rU();
                SkillSecondFloorRight();
            }
            else
            {
                basicSkill.rU();
                basicSkill.rU();
                basicSkill.tL();
                SkillSecondFloorLeft();
            }
        }
        else if (CheckCubeLocation(findColor.sideCubeColorList[8], checkCubeRight))
        {
            //Far
            if (findColor.GetCubeColor("Far", 1) == findColor.GetCubeColor("Near", 8))
            {
                basicSkill.rU();
                basicSkill.rU();
                SkillSecondFloorRight();
            }
            else
            {
                basicSkill.U();
                basicSkill.tL();
                SkillSecondFloorLeft();
            }
        }
        else if (CheckCubeLocation(findColor.sideCubeColorList[3], checkCubeRight))
        {
            // 3번 side cube
            if (findColor.GetCubeColor("Near", 7) == findColor.GetCubeColor("Near", 8))
            {
                //Nothing 정답!
                basicSkill.tL();
            }
            else
            {
                SkillSecondFloorRight();
            }
        }
        else if (CheckCubeLocation(findColor.sideCubeColorList[1], checkCubeRight))
        {
            // 1번 side cube
            SkillSecondFloorLeft();
        }
        else if (CheckCubeLocation(findColor.sideCubeColorList[9], checkCubeRight))
        {
            // 9번 side cube
            basicSkill.tL();
            SkillSecondFloorRight();
            basicSkill.tR();
        }
        else if (CheckCubeLocation(findColor.sideCubeColorList[11], checkCubeRight))
        {
            // 11번 side cube
            basicSkill.tR();
            SkillSecondFloorLeft();
            basicSkill.tL();
        }
    }


    //2번째 층이 제대로 맞춰졌는지 확인
    private bool SecondFlooClearCheck()
    {
        if(findColor.GetCubeColor("Right", 3) == findColor.GetCubeColor("Right", 8) &&
            findColor.GetCubeColor("Right", 7) == findColor.GetCubeColor("Right", 8) &&
            findColor.GetCubeColor("Left", 3) == findColor.GetCubeColor("Left", 8) &&
            findColor.GetCubeColor("Left", 7) == findColor.GetCubeColor("Left", 8) &&
            findColor.GetCubeColor("Near", 3) == findColor.GetCubeColor("Near", 8) &&
            findColor.GetCubeColor("Near", 7) == findColor.GetCubeColor("Near", 8) &&
            findColor.GetCubeColor("Far", 3) == findColor.GetCubeColor("Far", 8) &&
            findColor.GetCubeColor("Far", 7) == findColor.GetCubeColor("Far", 8) )
        {
            return true;
        }
        
        return false;
    }


    //맨 윗면의 십자가의 색 맞추기
    private void SetTopCrossStep()
    {
        InitBasicSkill();
        if (TopCrossStepClearCheck())
        {
            solverStep++;
            return;
        }
        else if (findColor.GetCubeColor("Top", 1) == findColor.topColor &&
            findColor.GetCubeColor("Top", 3) != findColor.topColor &&
            findColor.GetCubeColor("Top", 5) == findColor.topColor &&
            findColor.GetCubeColor("Top", 7) != findColor.topColor)
        {
            basicSkill.U();
        }
        else if (findColor.GetCubeColor("Top", 1) != findColor.topColor &&
            findColor.GetCubeColor("Top", 3) == findColor.topColor &&
            findColor.GetCubeColor("Top", 5) != findColor.topColor &&
            findColor.GetCubeColor("Top", 7) == findColor.topColor)
        {
            SkillTopCross();
        }
        else if (findColor.GetCubeColor("Top", 1) == findColor.topColor &&
            findColor.GetCubeColor("Top", 3) != findColor.topColor &&
            findColor.GetCubeColor("Top", 5) != findColor.topColor &&
            findColor.GetCubeColor("Top", 7) == findColor.topColor)
        {
            basicSkill.rU();
        }
        else if (findColor.GetCubeColor("Top", 1) != findColor.topColor &&
            findColor.GetCubeColor("Top", 3) == findColor.topColor &&
            findColor.GetCubeColor("Top", 5) == findColor.topColor &&
            findColor.GetCubeColor("Top", 7) != findColor.topColor)
        {
            basicSkill.U();
        }
        else if (findColor.GetCubeColor("Top", 1) != findColor.topColor &&
            findColor.GetCubeColor("Top", 3) != findColor.topColor &&
            findColor.GetCubeColor("Top", 5) == findColor.topColor &&
            findColor.GetCubeColor("Top", 7) == findColor.topColor)
        {
            basicSkill.U();
            basicSkill.U();
        }
        else if (findColor.GetCubeColor("Top", 1) == findColor.topColor &&
            findColor.GetCubeColor("Top", 3) == findColor.topColor &&
            findColor.GetCubeColor("Top", 5) != findColor.topColor &&
            findColor.GetCubeColor("Top", 7) != findColor.topColor)
        {
            SkillTopCross();
        }
        else if (findColor.GetCubeColor("Top", 1) != findColor.topColor &&
            findColor.GetCubeColor("Top", 3) != findColor.topColor &&
            findColor.GetCubeColor("Top", 5) != findColor.topColor &&
            findColor.GetCubeColor("Top", 7) != findColor.topColor)
        {
            SkillTopCross();
        }
    }


    //맨 윗면의 십자가의 색이 맞았는지 확인
    private bool TopCrossStepClearCheck()
    {
        if (findColor.GetCubeColor("Top", 1) == findColor.topColor &&
            findColor.GetCubeColor("Top", 3) == findColor.topColor &&
            findColor.GetCubeColor("Top", 5) == findColor.topColor &&
            findColor.GetCubeColor("Top", 7) == findColor.topColor )
        {
            return true;
        }

        return false;
    }


    //맨 윗면의 cornercube 위치의 색이 윗면의 색으로 설정
    private void SetTopCornerStep()
    {
        InitBasicSkill();
        if(TopCornerStepClearCheck())
        {
            solverStep++;
            //sixthStepFlag = true;
            return;
        }
        else if (findColor.GetCubeColor("Right", 0) == findColor.topColor &&
            findColor.GetCubeColor("Right", 2) == findColor.topColor &&
            findColor.GetCubeColor("Near", 2) == findColor.topColor &&
            findColor.GetCubeColor("Far", 0) == findColor.topColor)
        {
            SkillTopCorner();
        }
        else if (findColor.GetCubeColor("Near", 0) == findColor.topColor &&
            findColor.GetCubeColor("Near", 2) == findColor.topColor &&
            findColor.GetCubeColor("Far", 2) == findColor.topColor &&
            findColor.GetCubeColor("Far", 0) == findColor.topColor)
        {
            SkillTopCorner();
        }
        else if (findColor.GetCubeColor("Top", 2) == findColor.topColor &&
            findColor.GetCubeColor("Top", 6) == findColor.topColor &&
            findColor.GetCubeColor("Near", 2) == findColor.topColor &&
            findColor.GetCubeColor("Right", 0) == findColor.topColor)
        {
            SkillTopCorner();
        }
        else if (findColor.GetCubeColor("Top", 0) == findColor.topColor &&
            findColor.GetCubeColor("Right", 2) == findColor.topColor &&
            findColor.GetCubeColor("Near", 2) == findColor.topColor &&
            findColor.GetCubeColor("Left", 2) == findColor.topColor)
        {
            SkillTopCorner();
        }
        else if (findColor.GetCubeColor("Top", 0) == findColor.topColor &&
            findColor.GetCubeColor("Far", 0) == findColor.topColor &&
            findColor.GetCubeColor("Near", 0) == findColor.topColor &&
            findColor.GetCubeColor("Left", 0) == findColor.topColor)
        {
            SkillTopCorner();
        }
        else if (findColor.GetCubeColor("Top", 4) == findColor.topColor &&
            findColor.GetCubeColor("Top", 6) == findColor.topColor &&
            findColor.GetCubeColor("Far", 0) == findColor.topColor &&
            findColor.GetCubeColor("Far", 2) == findColor.topColor)
        {
            SkillTopCorner();
        }
        else if (findColor.GetCubeColor("Top", 0) == findColor.topColor &&
            findColor.GetCubeColor("Top", 6) == findColor.topColor &&
            findColor.GetCubeColor("Far", 0) == findColor.topColor &&
            findColor.GetCubeColor("Near", 2) == findColor.topColor)
        {
            SkillTopCorner();
        }
        else
        {
            basicSkill.U();
        }
    }
    

    //맨 윗면의 cornercube 위치의 색이 윗면의 색인지 확인
    private bool TopCornerStepClearCheck()
    {
        if (findColor.GetCubeColor("Top", 0) == findColor.topColor &&
            findColor.GetCubeColor("Top", 2) == findColor.topColor &&
            findColor.GetCubeColor("Top", 4) == findColor.topColor &&
            findColor.GetCubeColor("Top", 6) == findColor.topColor )
        {
            return true;
        }

        return false;
    }


    //맨 윗면의 sideCube 위치의 색을 맞게 조절
    private void SetThirdFloorSideCubeStep()
    {
        InitBasicSkill();
        if (ThirdFloorSideCubeStepClearCheck())
        {
            solverStep++;
           // seventhStepFlag = true;
            return;
        }



        List<string> checkCube = new List<string>();
        checkCube.Clear();
        checkCube.Add(findColor.GetCubeColor("Near", 8));
        checkCube.Add(findColor.GetCubeColor("Right", 8));
        checkCube.Add(findColor.topColor);

        findColor.SetCubeColorList();

        if (CheckCubeLocation(findColor.cornerCubeColorList[0], checkCube))
        {
            //Nothing
            basicSkill.tL();
        }
        else if (CheckCubeLocation(findColor.cornerCubeColorList[1], checkCube))
        {
            basicSkill.tR();
            SkillTopCornerCubeSideChange();
            basicSkill.tL();
        }
        else if (CheckCubeLocation(findColor.cornerCubeColorList[4], checkCube))
        {
            basicSkill.tL();
            SkillTopCornerCubeSideChange();
            basicSkill.tR();
            SkillTopCornerCubeSideChange();
        }
        else if (CheckCubeLocation(findColor.cornerCubeColorList[5], checkCube))
        {
            SkillTopCornerCubeSideChange();
        }


    }



    //맨 윗면의 sideCube 위치의 색을 맞는지 확인
    private bool ThirdFloorSideCubeStepClearCheck()
    {
        if (findColor.GetCubeColor("Right", 0) == findColor.GetCubeColor("Right", 8) &&
            findColor.GetCubeColor("Right", 2) == findColor.GetCubeColor("Right", 8) &&

            findColor.GetCubeColor("Left", 0) == findColor.GetCubeColor("Left", 8) &&
            findColor.GetCubeColor("Left", 2) == findColor.GetCubeColor("Left", 8) &&

            findColor.GetCubeColor("Near", 0) == findColor.GetCubeColor("Near", 8) &&
            findColor.GetCubeColor("Near", 2) == findColor.GetCubeColor("Near", 8) &&

            findColor.GetCubeColor("Far", 0) == findColor.GetCubeColor("Far", 8) &&
            findColor.GetCubeColor("Far", 2) == findColor.GetCubeColor("Far", 8) )
        {
            return true;
        }

        return false;
    }



    //맨 윗면의 cornerCube 위치의 색을 맞게 조절
    public void SetThirdFloorCornerCubeStep()
    {
        InitBasicSkill();
        if (ThirdFloorCornerCubeStepClearCheck())
        {
            solverStep = -1;
            return;
        }

        if(findColor.GetCubeColor("Near", 1) == findColor.GetCubeColor("Far", 8) &&
            findColor.GetCubeColor("Far", 1) == findColor.GetCubeColor("Right", 8) &&
            findColor.GetCubeColor("Right", 1) == findColor.GetCubeColor("Near", 8) )
        {
            SkillTopSideCubeChange();
        }
        else if (
            findColor.GetCubeColor("Near", 1) == findColor.GetCubeColor("Right", 8) &&
            findColor.GetCubeColor("Far", 1) == findColor.GetCubeColor("Near", 8) &&
            findColor.GetCubeColor("Right", 1) == findColor.GetCubeColor("Far", 8))
        {
            SkillTopSideCubeChange();
        }
        else if (
            findColor.GetCubeColor("Near", 1) == findColor.GetCubeColor("Far", 8) &&
            findColor.GetCubeColor("Far", 1) == findColor.GetCubeColor("Near", 8) &&
            findColor.GetCubeColor("Right", 1) == findColor.GetCubeColor("Left", 8) &&
            findColor.GetCubeColor("Left", 1) == findColor.GetCubeColor("Right", 8))
        {
            SkillTopSideCubeChange();
        }
        else if (
            findColor.GetCubeColor("Near", 1) == findColor.GetCubeColor("Right", 8) &&
            findColor.GetCubeColor("Right", 1) == findColor.GetCubeColor("Near", 8) &&
            findColor.GetCubeColor("Far", 1) == findColor.GetCubeColor("Left", 8) &&
            findColor.GetCubeColor("Left", 1) == findColor.GetCubeColor("Far", 8))
        {
            SkillTopSideCubeChange();
        }
        else
        {
            basicSkill.tL();
        }
    }


    //맨 윗면의 cornerCube 위치의 색을 맞는지 확인
    private bool ThirdFloorCornerCubeStepClearCheck()
    {
        if (findColor.GetCubeColor("Right", 0) == findColor.GetCubeColor("Right", 8) &&
            findColor.GetCubeColor("Right", 1) == findColor.GetCubeColor("Right", 8) &&
            findColor.GetCubeColor("Right", 2) == findColor.GetCubeColor("Right", 8) &&

            findColor.GetCubeColor("Left", 0) == findColor.GetCubeColor("Left", 8) &&
            findColor.GetCubeColor("Left", 1) == findColor.GetCubeColor("Left", 8) &&
            findColor.GetCubeColor("Left", 2) == findColor.GetCubeColor("Left", 8) &&

            findColor.GetCubeColor("Near", 0) == findColor.GetCubeColor("Near", 8) &&
            findColor.GetCubeColor("Near", 1) == findColor.GetCubeColor("Near", 8) &&
            findColor.GetCubeColor("Near", 2) == findColor.GetCubeColor("Near", 8) &&

            findColor.GetCubeColor("Far", 0) == findColor.GetCubeColor("Far", 8) &&
            findColor.GetCubeColor("Far", 1) == findColor.GetCubeColor("Far", 8) &&
            findColor.GetCubeColor("Far", 2) == findColor.GetCubeColor("Far", 8))
        {
            return true;
        }

        return false;
    }


    private void SkilltUtU()
    {
        basicSkill.tU();
        basicSkill.tU();
    }

    private void SkilltL()
    {
        basicSkill.tL();
    }

    private void SkillTopSideCubeChange()
    {
        basicSkill.R();
        basicSkill.R();
        basicSkill.U();
        basicSkill.rcF();
        basicSkill.U();
        basicSkill.U();
        basicSkill.cF();
        basicSkill.U();
        basicSkill.R();
        basicSkill.R();
    }
    

    private void SkillTopCornerCubeSideChange()
    {
        basicSkill.R();
        basicSkill.U();
        basicSkill.U();
        basicSkill.rR();
        basicSkill.rU();
        basicSkill.R();
        basicSkill.U();
        basicSkill.U();

        basicSkill.rcR();
        basicSkill.rR();
        basicSkill.F();
        basicSkill.rR();
        basicSkill.rF();
        basicSkill.cR();
        basicSkill.R();
    }


    private void SkillTopCorner()
    {
        basicSkill.cR();
        basicSkill.R();
        basicSkill.U();
        basicSkill.rR();
        basicSkill.rU();

        basicSkill.rcR();
        basicSkill.rR();
        basicSkill.F();
        basicSkill.R();
        basicSkill.rF();

    }



    private void SkillSecondFloorRight()
    {
        basicSkill.U();
        basicSkill.R();
        basicSkill.rU();
        basicSkill.rR();
        basicSkill.rU();
        basicSkill.rF();
        basicSkill.U();
        basicSkill.F();
    }
    private void SkillSecondFloorLeft()
    {
        basicSkill.rU();
        basicSkill.rL();
        basicSkill.U();
        basicSkill.L();
        basicSkill.U();
        basicSkill.F();
        basicSkill.rU();
        basicSkill.rF();
    }

    private void SkillTopCross()
    {
        basicSkill.F();
        basicSkill.R();
        basicSkill.U();
        basicSkill.rR();
        basicSkill.rU();
        basicSkill.rF();
    }
    private void SkillTopCrossChange()
    {
        basicSkill.R();
        basicSkill.U();
        basicSkill.rR();
        basicSkill.U();
        basicSkill.R();
        basicSkill.U();
        basicSkill.U();
        basicSkill.rR();
        basicSkill.U();
    }



    private void SkillRUrRrU()
    {
        basicSkill.R();
        basicSkill.U();
        basicSkill.rR();
        basicSkill.rU();
    }

    private void SkillrLrULU()
    {
        basicSkill.rL();
        basicSkill.rU();
        basicSkill.L();
        basicSkill.U();
    }


    private void InitBasicSkill()
    {
        basicSkill.lineList.Clear();
        basicSkill.directionList.Clear();
    }
}
