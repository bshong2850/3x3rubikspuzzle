using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RandomSuffle : MonoBehaviour
{
    
    public static int randomLineUnionChooseValue;
    public static int randomLineValue;
    public static int randomDirectionValue;
    public static int randomSize = 50;
    public static int randomCount = 0;

    public static bool randomShuffleFlag = false;
    public static void RandomSuffleSetting()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            RandomValueSetting();
            randomShuffleFlag = true;
        }
    }

    public static void RandomValueSetting()
    {
        randomLineUnionChooseValue = UnityEngine.Random.Range(0, 10);   // 7 이상은 Union Rotation, 이하는 Line Rotation
        randomLineValue = UnityEngine.Random.Range(0, 9);               // Random으로 Line 선택
        randomDirectionValue = UnityEngine.Random.Range(0, 2);          // Random으로 Direction 선택
    }

    public static void RandomDirectionSetting()
    {
        /*
                   q  w  e 
                a--|--|--|       randomValue =   0   1   2   3   4   5
                s--|--|--|       line        =   a   s   d   q   w   e
                d--|--|--|

                z = far     6
                x = center  7
                c = near    8
            
                dvalue      =   0       1       2       3       4       5
                direction   =   U       D       R       L       sR      sL
                vector      =  left   right     up     down    back    front
        */
        if (randomLineValue == 0 || randomLineValue == 1 || randomLineValue == 2)                // Line = q or a or z
        {
            if(randomDirectionValue == 0)
            {
                RubiksCube.rotationAxis = Vector3.down;                 // Direction = Right
            }
            if (randomDirectionValue == 1)
            {
                RubiksCube.rotationAxis = Vector3.up;                   // Direction = Left
            }
        }
        else if (randomLineValue == 3 || randomLineValue == 4 || randomLineValue == 5)          // Line = r or e or w
        {
            if (randomDirectionValue == 0)
            {
                RubiksCube.rotationAxis = Vector3.right;                // Direction = Up
            }
            if (randomDirectionValue == 1)
            {
                RubiksCube.rotationAxis = Vector3.left;                 // Direction = Down
            }
        }
        else                                                            // Line = n or f
        {
            if (randomDirectionValue == 0)
            {
                RubiksCube.rotationAxis = Vector3.back;              // Direction = Stacked Right
            }
            if (randomDirectionValue == 1)
            {
                RubiksCube.rotationAxis = Vector3.forward;                 // Direction = Stacked Left
            }
        }
    }
}