﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pair<T, U>
{
    public Pair()
    {
    }

    public Pair(T first, U second)
    {
        this.First = first;
        this.Second = second;
    }

    public T First { get; set; }
    public U Second { get; set; }
};

public class FindColor
{
    private float FLOAT_EPSLION_ = 1.19209290E-07F;
    public List<Pair<string, int>> findColorLocationList = new List<Pair<string, int>>();


    public string topColor = "Yellow";
    public string bottomColor = "White";

    //Center, Side, Corner Cube들의 번호에 따른 Color 저장
    public string[] centerCubeColorList = new string[6];
    public List<List<string>> cornerCubeColorList = new List<List<string>>();
    public List<List<string>> sideCubeColorList = new List<List<string>>();


    //Big Face의 이름들 선언
    private string[] bigFaceName = new string[] { "Top", "Bottom", "Right", "Left", "Far", "Near" };
    
    //GetBigFace
    // 원하는 Big Face의 이름과 pos가 주어졌을 때 해당하는 BigFace Return
    public List<GameObject> GetBigFace(string bFN)
    {
        List<GameObject> c = new List<GameObject>();

        if (bFN == bigFaceName[0])           // Top
        {
            c = RubiksCube.unionCube_.FindAll(x => Math.Abs(1 - x.transform.GetChild(0).position.y) < FLOAT_EPSLION_);
        }
        else if (bFN == bigFaceName[1])      // Bottom
        {
            c = RubiksCube.unionCube_.FindAll(x => Math.Abs(-1 - x.transform.GetChild(0).position.y) < FLOAT_EPSLION_);
        }
        else if (bFN == bigFaceName[2])      // Right
        {
            c = RubiksCube.unionCube_.FindAll(x => Math.Abs(1 - x.transform.GetChild(0).position.x) < FLOAT_EPSLION_);
        }
        else if (bFN == bigFaceName[3])      // Left
        {
            c = RubiksCube.unionCube_.FindAll(x => Math.Abs(-1 - x.transform.GetChild(0).position.x) < FLOAT_EPSLION_);
        }
        else if (bFN == bigFaceName[4])      // Far
        {
            c = RubiksCube.unionCube_.FindAll(x => Math.Abs(1 - x.transform.GetChild(0).position.z) < FLOAT_EPSLION_);
        }
        else if (bFN == bigFaceName[5])      // Near
        {
            c = RubiksCube.unionCube_.FindAll(x => Math.Abs(-1 - x.transform.GetChild(0).position.z) < FLOAT_EPSLION_);
        }
        return c;
    }

    //ComputeComparePos
    // 인자들에 맞는 comparePos return
    public float ComputeComparePos(string fN, List<GameObject> c, int i, int numchild)
    {
        float comparePos = 0.0f;

        if (fN == bigFaceName[0] || fN == bigFaceName[1])
        {
            comparePos = c[i].transform.GetChild(numchild).position.y;
        }
        else if (fN == bigFaceName[2] || fN == bigFaceName[3])
        {
            comparePos = c[i].transform.GetChild(numchild).position.x;
        }
        else if (fN == bigFaceName[4] || fN == bigFaceName[5])
        {
            comparePos = c[i].transform.GetChild(numchild).position.z;
        }

        return comparePos;
    }

    //ComputeColorSmallFaceIndex
    // bFN에서 pos에 맞는 index를 return
    public int ComputeColorSmallFaceIndex(string bFN, Vector3 pos)
    {
        float pos1 = 0.0f;
        float pos2 = 0.0f;
        if (bFN == bigFaceName[0])
        {
            //Top
            pos1 = pos.x;
            pos2 = pos.z;
        }
        else if (bFN == bigFaceName[1])
        {
            //Bottom
            pos1 = -pos.x;
            pos2 = pos.z;
        }
        else if (bFN == bigFaceName[2])
        {
            //Right
            pos1 = pos.z;
            pos2 = pos.y;
        }
        else if (bFN == bigFaceName[3])
        {
            //Left
            pos1 = -pos.z;
            pos2 = pos.y;
        }
        else if (bFN == bigFaceName[4])
        {
            //Far
            pos1 = -pos.x;
            pos2 = pos.y;
        }
        else if (bFN == bigFaceName[5])
        {
            //Near
            pos1 = pos.x;
            pos2 = pos.y;
        }

        //    Near Right Top               Far Left Bottom
        //    ---- ---- ----               ---- ---- ----
        //   | 2  | 1  | 0  |             | 0  | 1  | 2  |
        //    ---- ---- ----               ---- ---- ---- 
        //   | 3  | 8  | 7  |             | 7  | 8  | 3  |
        //    ---- ---- ----               ---- ---- ----
        //   | 4  | 5  | 6  |             | 6  | 5  | 4  |
        //    ---- ---- ----               ---- ---- ----

        if (pos1 > 0.5f && pos2 > 0.5f)
            return 0;
        else if (pos1 < 0.5f && pos1 > -0.5f && pos2 > 0.5f)
            return 1;
        else if (pos1 < -0.5f && pos2 > 0.5f)
            return 2;
        else if (pos1 > 0.5f && pos2 < 0.5f && pos2 > -0.5f)
            return 7;
        else if (pos1 < 0.5f && pos1 > -0.5f && pos2 < 0.5f && pos2 > -0.5f)
            return 8;
        else if (pos1 < -0.5f && pos2 < 0.5f && pos2 > -0.5f)
            return 3;
        else if (pos1 > 0.5f && pos2 < -0.5f)
            return 6;
        else if (pos1 < 0.5f && pos1 > -0.5f && pos2 < -0.5f)
            return 5;
        else if (pos1 < -0.5f && pos2 < -0.5f)
            return 4;

        return -1;
    }



    //BigFace와 BigFace의 Name 받아와 Display되고 있는 Small Face를 Return
    public List<GameObject> GetDisplaySmallFace(List<GameObject> bF, string fN)
    {
        List<GameObject> c = new List<GameObject>();
        float comparePos = 0.0f;
        float pos = 0.0f;

        if (fN == bigFaceName[0] || fN == bigFaceName[2] || fN == bigFaceName[4])
        {
            pos = 1.1f;
        }
        else
        {
            pos = -1.1f;
        }

        for (int i = 0; i < bF.Count; ++i)
        {
            for (int numchild = 0; numchild < bF[i].transform.childCount; ++numchild)
            {
                // BigFace의 i번째 Face에서 numchild번째의 Cube에 해당하는 comparePos를 구함
                comparePos = ComputeComparePos(fN, bF, i, numchild);
                if (pos - comparePos < FLOAT_EPSLION_ &&
                    pos - comparePos > -FLOAT_EPSLION_)
                {
                    //만족한다면 c에 Add
                    c.Add(bF[i].transform.GetChild(numchild).gameObject);
                    //Debug.Log(bF[i].transform.GetChild(numchild).gameObject.name);
                }
            }
        }

        return c;
    }


    //FindDisplayFace
    // Display된 Face 받아오기
    List<GameObject> FindDisplayFace(string bFN)
    {
        //Get Bottom Big Face
        List<GameObject> bigFace = new List<GameObject>();
        bigFace = GetBigFace(bFN);

        //Get Bottom Display Small Face
        List<GameObject> displaySmallFace = new List<GameObject>();
        displaySmallFace = GetDisplaySmallFace(bigFace, bFN);

        return displaySmallFace;
    }

    /////////////////////////////////////// Total Face ////////////////////////////////////////////////////

    //FindTargetColorInBigFace
    // bFN에 해당하는 bigFace에 color가 있으면 findColorLocationList 추가
    public void FindTargetColorInBigFace(string bFN, string color)
    {
        List<GameObject> displaySmallFace = FindDisplayFace(bFN);
        for (int i = 0; i < displaySmallFace.Count; ++i)
        {
            if (displaySmallFace[i].name == color)
            {
                //Compute Index
                int index = ComputeColorSmallFaceIndex(bFN, displaySmallFace[i].transform.position);
                Pair<string, int> colorPosition = new Pair<string, int>(bFN, index);
                findColorLocationList.Add(colorPosition);
            }
        }
    }



    //SetColorLocation
    // 모든 bigFaceName을 돌면서 각각의 bigFace에서 color 위치를 찾아서 저장
    public void FindColorLocation(string color)
    {
        findColorLocationList.Clear();
        for (int i = 0; i < bigFaceName.Length; ++i)
        {
            FindTargetColorInBigFace(bigFaceName[i], color);
        }
    }

    /////////////////////////////////////// Cube Color ////////////////////////////////////////////////////
    //InitCubeColorList
    // cneter, corner, side Cube를 Initialize
    void InitCubeColorList()
    {
        for (int i = 0; i < 6; ++i)
        {
            centerCubeColorList[i] = " ";
        }

        cornerCubeColorList.Clear();
        for (int i = 0; i < 8; ++i)
        {
            List<string> oneCubeColor = new List<string>();
            cornerCubeColorList.Add(oneCubeColor);
        }
        sideCubeColorList.Clear();
        for (int i = 0; i < 12; ++i)
        {
            List<string> oneCubeColor = new List<string>();
            sideCubeColorList.Add(oneCubeColor);
        }
    }

    //SetCubeDisplayColor
    // big Face Name이 주어졌을 때 그 big Face의 Display Color를 center, corner, side로 나누어 저장
    void SetCubeDisplayColor(string bFN)
    {

        //    Near Right Top               Far Left Bottom
        //    ---- ---- ----               ---- ---- ----
        //   | 2  | 1  | 0  |             | 0  | 1  | 2  |
        //    ---- ---- ----               ---- ---- ---- 
        //   | 3  | 8  | 7  |             | 7  | 8  | 3  |
        //    ---- ---- ----               ---- ---- ----
        //   | 4  | 5  | 6  |             | 6  | 5  | 4  |
        //    ---- ---- ----               ---- ---- ----

        //Get Big Face
        List<GameObject> bigFace = new List<GameObject>();
        bigFace = GetBigFace(bFN);

        //Get Small Face
        List<GameObject> displaySmallFace = new List<GameObject>();
        displaySmallFace = GetDisplaySmallFace(bigFace, bFN);

        for (int i = 0; i < displaySmallFace.Count; ++i)
        {
            int index = ComputeColorSmallFaceIndex(bFN, displaySmallFace[i].transform.position);

            //center
            if (bFN == "Top" && index == 8)
                centerCubeColorList[0] = displaySmallFace[i].name;
            if (bFN == "Bottom" && index == 8)
                centerCubeColorList[1] = displaySmallFace[i].name;
            if (bFN == "Right" && index == 8)
                centerCubeColorList[2] = displaySmallFace[i].name;
            if (bFN == "Left" && index == 8)
                centerCubeColorList[3] = displaySmallFace[i].name;
            if (bFN == "Far" && index == 8)
                centerCubeColorList[4] = displaySmallFace[i].name;
            if (bFN == "Near" && index == 8)
                centerCubeColorList[5] = displaySmallFace[i].name;

            //corner 0
            if (bFN == "Near" && index == 0)
                cornerCubeColorList[0].Add(displaySmallFace[i].name);
            if (bFN == "Top" && index == 6)
                cornerCubeColorList[0].Add(displaySmallFace[i].name);
            if (bFN == "Right" && index == 2)
                cornerCubeColorList[0].Add(displaySmallFace[i].name);

            //corner 1
            if (bFN == "Near" && index == 2)
                cornerCubeColorList[1].Add(displaySmallFace[i].name);
            if (bFN == "Top" && index == 4)
                cornerCubeColorList[1].Add(displaySmallFace[i].name);
            if (bFN == "Left" && index == 0)
                cornerCubeColorList[1].Add(displaySmallFace[i].name);

            //corner 2
            if (bFN == "Near" && index == 4)
                cornerCubeColorList[2].Add(displaySmallFace[i].name);
            if (bFN == "Bottom" && index == 6)
                cornerCubeColorList[2].Add(displaySmallFace[i].name);
            if (bFN == "Left" && index == 6)
                cornerCubeColorList[2].Add(displaySmallFace[i].name);

            //corner 3
            if (bFN == "Near" && index == 6)
                cornerCubeColorList[3].Add(displaySmallFace[i].name);
            if (bFN == "Bottom" && index == 4)
                cornerCubeColorList[3].Add(displaySmallFace[i].name);
            if (bFN == "Right" && index == 4)
                cornerCubeColorList[3].Add(displaySmallFace[i].name);

            //corner 4
            if (bFN == "Far" && index == 0)
                cornerCubeColorList[4].Add(displaySmallFace[i].name);
            if (bFN == "Top" && index == 2)
                cornerCubeColorList[4].Add(displaySmallFace[i].name);
            if (bFN == "Left" && index == 2)
                cornerCubeColorList[4].Add(displaySmallFace[i].name);

            //corner 5
            if (bFN == "Far" && index == 2)
                cornerCubeColorList[5].Add(displaySmallFace[i].name);
            if (bFN == "Top" && index == 0)
                cornerCubeColorList[5].Add(displaySmallFace[i].name);
            if (bFN == "Right" && index == 0)
                cornerCubeColorList[5].Add(displaySmallFace[i].name);

            //corner 6
            if (bFN == "Far" && index == 4)
                cornerCubeColorList[6].Add(displaySmallFace[i].name);
            if (bFN == "Bottom" && index == 2)
                cornerCubeColorList[6].Add(displaySmallFace[i].name);
            if (bFN == "Right" && index == 6)
                cornerCubeColorList[6].Add(displaySmallFace[i].name);

            //corner 7
            if (bFN == "Far" && index == 6)
                cornerCubeColorList[7].Add(displaySmallFace[i].name);
            if (bFN == "Bottom" && index == 0)
                cornerCubeColorList[7].Add(displaySmallFace[i].name);
            if (bFN == "Left" && index == 4)
                cornerCubeColorList[7].Add(displaySmallFace[i].name);


            //side 0
            if (bFN == "Near" && index == 1)
                sideCubeColorList[0].Add(displaySmallFace[i].name);
            if (bFN == "Top" && index == 5)
                sideCubeColorList[0].Add(displaySmallFace[i].name);
            //side 1
            if (bFN == "Near" && index == 3)
                sideCubeColorList[1].Add(displaySmallFace[i].name);
            if (bFN == "Left" && index == 7)
                sideCubeColorList[1].Add(displaySmallFace[i].name);
            //side 2
            if (bFN == "Near" && index == 5)
                sideCubeColorList[2].Add(displaySmallFace[i].name);
            if (bFN == "Bottom" && index == 5)
                sideCubeColorList[2].Add(displaySmallFace[i].name);
            //side 3
            if (bFN == "Near" && index == 7)
                sideCubeColorList[3].Add(displaySmallFace[i].name);
            if (bFN == "Right" && index == 3)
                sideCubeColorList[3].Add(displaySmallFace[i].name);

            //side 4
            if (bFN == "Right" && index == 1)
                sideCubeColorList[4].Add(displaySmallFace[i].name);
            if (bFN == "Top" && index == 7)
                sideCubeColorList[4].Add(displaySmallFace[i].name);
            //side 5
            if (bFN == "Left" && index == 1)
                sideCubeColorList[5].Add(displaySmallFace[i].name);
            if (bFN == "Top" && index == 3)
                sideCubeColorList[5].Add(displaySmallFace[i].name);
            //side 6
            if (bFN == "Left" && index == 5)
                sideCubeColorList[6].Add(displaySmallFace[i].name);
            if (bFN == "Bottom" && index == 7)
                sideCubeColorList[6].Add(displaySmallFace[i].name);
            //side 7
            if (bFN == "Right" && index == 5)
                sideCubeColorList[7].Add(displaySmallFace[i].name);
            if (bFN == "Bottom" && index == 3)
                sideCubeColorList[7].Add(displaySmallFace[i].name);

            //side 8
            if (bFN == "Far" && index == 1)
                sideCubeColorList[8].Add(displaySmallFace[i].name);
            if (bFN == "Top" && index == 1)
                sideCubeColorList[8].Add(displaySmallFace[i].name);
            //side 9
            if (bFN == "Far" && index == 3)
                sideCubeColorList[9].Add(displaySmallFace[i].name);
            if (bFN == "Right" && index == 7)
                sideCubeColorList[9].Add(displaySmallFace[i].name);
            //side 10
            if (bFN == "Far" && index == 5)
                sideCubeColorList[10].Add(displaySmallFace[i].name);
            if (bFN == "Bottom" && index == 1)
                sideCubeColorList[10].Add(displaySmallFace[i].name);
            //side 11
            if (bFN == "Far" && index == 7)
                sideCubeColorList[11].Add(displaySmallFace[i].name);
            if (bFN == "Left" && index == 3)
                sideCubeColorList[11].Add(displaySmallFace[i].name);

        }
    }

    //SetCubeColorList
    //모든 bigFaceName을 돌면서 centerCube, cornerCube, sideCube를 저장
    public void SetCubeColorList()
    {
        InitCubeColorList();
        for (int i = 0; i < bigFaceName.Length; ++i)
        {
            SetCubeDisplayColor(bigFaceName[i]);
        }
    }


    /////////////////////////////////////// Find Face Color ////////////////////////////////////////////////////
    //GetCubeColor
    // big Face Name과 index가 정해졌을 때 그 위치의 color를 return해준다.
    public string GetCubeColor(string bFN, int index)
    {

        List<GameObject> displaySmallFace = FindDisplayFace(bFN);

        for (int i = 0; i < displaySmallFace.Count; ++i)
        {
            if (index == ComputeColorSmallFaceIndex(bFN, displaySmallFace[i].transform.position))
            {
                return displaySmallFace[i].name;
            }
        }

        return " ";
    }

    //GetFaceName
    // color가 정해졌을 때 그 color를 center로 가지는 big Face Name을 return해준다.
    public string GetFaceName(string color)
    {
        SetCubeColorList();
        for(int i = 0; i<6; ++i)
        {
            if (centerCubeColorList[i] == color)
                return bigFaceName[i];
        }
        return " ";
    }


}
